package com.virtualmind;

import com.virtualmind.bd.IUtil;
import com.virtualmind.bd.Util;
import com.virtualmind.entity.Post;
import com.virtualmind.entity.Topic;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	IUtil iUtil = new Util();
    	
    	// Test for Topic
        Topic t = new Topic();
        t.setId(1);
        t.setName("Topic from DB");
        
        iUtil.persistirObjeto(t);
        
        // Test for Post
        Post p = new Post();
        p.setTopic(t);
        p.setTitle("Title for 12 topic");
        p.setText("Title for 12 topic");
        
        iUtil.persistirObjeto(p);
    }
}

package com.virtualmind.bd;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class Util implements IUtil, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Object persistirObjeto(Object objeto){
		EntityManager em = Conexion.getInstance().getEm();
        EntityTransaction tx = em.getTransaction();
        try{
        	tx.begin();
        	Object obj = em.merge(objeto);
        	tx.commit();
        	return obj;
        }catch (Exception e2) {
        	e2.printStackTrace();
			if(tx.isActive()){
				tx.rollback();
			}
			return null;
		}finally {
			em.close();
		}
	}
}

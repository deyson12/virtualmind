package com.virtualmind.bd;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Conexion {
	
	private static Conexion con = null;
	
	//Entity Manager for Virtualmind BD
	private EntityManagerFactory emf;
    
    public static Conexion getInstance(){
    	if(con==null){
    		con = new Conexion();
    	}
    	return con;
    }
    
    public EntityManager getEm() {
        if (con.emf == null) {
        	con.emf = Persistence.createEntityManagerFactory("virtualmind");
        }
        return con.emf.createEntityManager();
    }
}
